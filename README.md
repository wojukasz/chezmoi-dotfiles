### ZSH
chsh and point it to zshell path, for example `/usr/bin/zsh `
run: chsh -s $(which zsh)

### Install dependency for vimrc and zsh
sudo pip3 install neovim
sudo dnf install zsh

### ZSH Plugin instalation
zplug install

### chezmoi
1. install chezmoi https://github.com/twpayne/chezmoi/blob/master/docs/INSTALL.md
2. chezmoi init <url> for a new machine
chezmoi init git@gitlab.com:wojukasz/chezmoi-dotfiles.git

3. chezmoi apply after pull

chezmoi cd - jump to configuration repo
