# {{{ .bashrc
# .bashrc

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# {{{ Bash helper functions
if [ ! -e "$HOME/.local/share" ]; then
  mkdir -p "$HOME/.local/share"
fi
# }}}

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"
# }}}

# {{{ Enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# alias cdd="cd ~/Downloads/"
alias cdg="cd ~/git/"
alias cdot="chezmoi cd"

alias codebuild="~/git/aws-codebuild-docker-images/local_builds/codebuild_build.sh"
alias fpm='docker run --rm -v "${WORKSPACE}:/source" -v /etc/passwd:/etc/passwd:ro --user=$(id -u):$(id -g) claranet/fpm'
alias tmux='direnv exec / tmux' # https://github.com/direnv/direnv/wiki/Tmux

alias vim="nvim"
alias vi="nvim"

alias docker_clean_images='docker rmi $(docker images -a --filter=dangling=true -q)'
alias docker_clean_ps='docker rm $(docker ps --filter=status=exited --filter=status=created -q)'

# todo: https://itnext.io/pimp-my-kubernetes-shell-f144710232a0<Paste>
# }}}

# {{{ Easier navigation
## .., ..., ...., ....., and -
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."
alias -- -="cd -"
# }}}

# {{{ Environment variables and exported paths
export AWS_DEFAULT_REGION="eu-west-1"
export EDITOR="vim"
export CHEATCOLORS=true                 # cheat needs to be installed https://github.com/chrisallenlane/cheat
# }}}

# {{{ Installations

# {{{ Asdf and plugins installation: https://github.com/asdf-vm/asdf
ASDF_DIR="$HOME/.asdf"
if [ ! -e "$ASDF_DIR" ]; then
  git clone https://github.com/asdf-vm/asdf.git "$ASDF_DIR" --branch v0.6.3
fi

ASDF_PLUGINS=(
  # kops
  # kube-capacity
  # kubectl
  # kubectl-bindrole
  # kubefedctl
  # kubeseal
  # kubesec
  # kubeval
  # ruby
  # terraform
  # terraform-validator
  # tflint
)

for plugin in "${ASDF_PLUGINS[@]}"; do
  if [ ! -e "${ASDF_DIR}/plugins/${plugin}" ]; then
    asdf plugin-add "${plugin}"
    ASDF_LATEST=$(asdf list-all "${plugin}" | tail -n1)
    asdf install "${plugin}" "${ASDF_LATEST}"
    asdf global "${plugin}" "${ASDF_LATEST}"
    unset ASDF_LATEST
  fi
done
# }}}

# {{{ base16 installation
BASE16_SHELL="$HOME/.config/base16-shell/"
if [ ! -e "${BASE16_SHELL}" ]; then
  git clone git@github.com:chriskempson/base16-shell.git "${BASE16_SHELL}"
  # chmod 600 /home/$USER/.ssh/config
fi
# }}}

# {{{ Bash preexec
BASH_PREEXEC="$HOME/.local/share/bash-preexec"
if [ ! -e "${BASH_PREEXEC}" ]; then
  git clone https://github.com/rcaloras/bash-preexec.git "${BASH_PREEXEC}"
fi
# }}}

# {{{ FASD Initialisation
# if command -v fasd &>/dev/null; then
#   eval "$(fasd --init auto)"
# fi
# }}}

# {{{ Direnv setup: https://github.com/direnv/direnv
eval "$(direnv hook bash)"
# if command -v direnv; then
#   eval "$(direnv hook bash)"
# fi
# }}}

# }}}

# {{{ Source existing files bash helpers
SOURCE_FILES=(
    # /usr/share/bash-completion/bash_completion
    # /usr/share/doc/pkgfile/command-not-found.bash
    # /usr/share/git/completion/git-completion.bash
    # /usr/local/git/contrib/completion/git-completion.bash
    # /usr/share/git/completion/git-prompt.sh
    # /usr/local/etc/bash_completion.d/git-prompt.sh
    # /usr/lib/ruby/gems/2.5.0/gems/tmuxinator-0.12.0/completion/tmuxinator.bash
    "$HOME/.asdf/completions/asdf.bash"
    "$HOME/.asdf/asdf.sh"
    "${BASH_PREEXEC}/bash-preexec.sh"
     # /usr/share/bash-completion/completions/aws_bash_completer # aws cli completer
)

for FILE in "${SOURCE_FILES[@]}";
do
    if [ -e "$FILE" ];
    then
        source "$FILE"
    fi
done
# }}}

# {{{ ranger cd
function rcd {
    # create a temp file and store the name
    tempfile="$(mktemp -t tmp.XXXXXX)"

    # run ranger and ask it to output the last path into the
    # temp file
    ranger --choosedir="$tempfile" "${@:-$(pwd)}"

    # if the temp file exists read and the content of the temp
    # file was not equal to the current path
    test -f "$tempfile" &&
    if [ "$(cat -- "$tempfile")" != "$(echo -n `pwd`)" ]; then
        # change directory to the path in the temp file
        cd -- "$(cat "$tempfile")"
    fi

    # its not super necessary to have this line for deleting
    # the temp file since Linux should handle it on the next
    # boot
    rm -f -- "$tempfile"
}
# }}}

[ -f ~/.fzf.bash ] && source ~/.fzf.bash

### Terminal GUI

# ADDING SUPPORT FOR 256 colors
export TERM=xterm-256color

# {{{ base16 colour theme setup
[ -n "$PS1" ] && \
[ -s "$BASE16_SHELL/profile_helper.sh" ] && \
eval "$("$BASE16_SHELL/profile_helper.sh")"
# }}}

# {{{ Go setup
# export GOPATH=$HOME/dev/go
# }}}

# {{{ MISC
# http://dev.nuclearrooster.com/2009/12/07/quick-download-benchmarks-with-curl/
alias dl='curl --silent -w "Total:          %{time_total}\nDNS:            %{time_namelookup}\nConnect:        %{time_connect}\nPretransfer:    %{time_pretransfer}\nRedirect:       %{time_redirect}\nStart Transfer: %{time_starttransfer} \n" --output /dev/null '

# Yotube music
alias convert="youtube-dl --extract-audio --audio-format mp3 --audio-quality 0" # convert yt to best quality, just add url at the end
alias listen="youtube-dl -o - "$1" | mpv --no-video -"

# xset -dpms # Prevent monitor from losing signal after screen saver / lock activates
# }}}
